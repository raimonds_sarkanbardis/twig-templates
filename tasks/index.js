import gulp from 'gulp';
import templateTask from './template';

export const template = gulp.series(templateTask);
export default template;
