import webpack from 'webpack';
import paths from '../core/paths';

import ExtractTextPlugin from 'extract-text-webpack-plugin';


module.exports = {
  entry: [
    'webpack-hot-middleware/client',
    paths.mainJS
  ],

  output: {
    filename: 'bundle.js',
    path: paths.public,
    publicPath: '/'
  },

  resolve: {
    extensions: ['.js', '.jsx', '.css', '.scss'],
    modules: [paths.appSrc, 'node_modules']
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              modules: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          // resolve-url-loader may be chained before sass-loader if necessary
          use: [{
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }, {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }]
        })
      },
      /* {
        enforce: 'pre',
        test: /\.(js|jsx)$/,
        include: paths.scripts,
        loader: 'eslint-loader',
      }, */
      {
        test: /\.(js|jsx)$/,
        include: paths.scripts,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true
        }
      },
      {
        test: [/\.jpe?g$/, /\.png$/, /\.gif$/],
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'static/images/[name].[hash:8].[ext]'
        }
      },
      {
        exclude: [
          /\.s?css$/,
          /\.(js|jsx)$/,

          /\.jpe?g$/,
          /\.png$/,
          /\.gif$/,

          /\.json$/,
          /\.md$/
        ],
        loader: 'file-loader',
        options: {
          name: 'static/[name].[hash:8].[ext]'
        }
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin({
      // options
    }),
    new ExtractTextPlugin({
      filename: 'css/[name].[contenthash].css',
      disable: process.env.NODE_ENV !== 'production'
    })
  ]
};
