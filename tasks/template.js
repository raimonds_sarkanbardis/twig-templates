import Twig from 'twig';
import getComponent from '../core/utils/getComponent';

const template = (callback) => {
  const Home = getComponent('Home');
  Twig.renderFile(Home.template, Home.data, (error, html) => console.log(html));
  callback();
}

export default template;
