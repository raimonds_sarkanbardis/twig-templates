import express from 'express';
import twig from 'twig';
import path from 'path';
import paths from '../core/paths';
import routes from '../core/routes';
import chalk from 'chalk';

import fs from 'fs';
import chokidar from 'chokidar';
import livereload from 'livereload';
import livereloadMiddleware  from 'connect-livereload';

import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from './webpack.config';


const app = express();
const port = process.env.PORT || 3000;
app.set('port', port);


app.set('view engine', 'twig');
app.set('view options', { layout: false });
app.set('twig options', {
  namespaces: {
    public: `${paths.public}/`,
    templates: `${paths.templates}/`,
    blocks: `${paths.blocks}/`,
    pages: `${paths.pages}/`
  },
  strict_variables: false
});
twig.cache(false);


const compiler = webpack(webpackConfig);
const middleware = webpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  stats: {
    colors: true,
    errors: true,
    modules: false,
    performance: true,
    timings: true,
    warnings: true
  }
});
app.use(middleware);
app.use(webpackHotMiddleware(compiler, {
  log: console.log
}));
app.use(express.static(paths.public));


const liveServer = livereload.createServer({
  exts: ['json', 'twig'],
});
liveServer.watch(paths.templates);
app.use(livereloadMiddleware());


const renderNotFound = (req, res) => {
  // FIXME: Pārbaudam vai pieprasījums nav pēc faila
  if (!req.path.includes('.')) {
    console.log(`${chalk.yellow('WARN')} Template for path ${chalk.magenta(req.path)} not found`);
  }
  res.status(404).end();
}

routes.map(route => {
  app.get(route.path || '/', (req, res) => {
    // FIXME: Vai data ir nepieciešams, ja pieprasam caur update?
    const {template, data, update} = route.component;
    if (template) {
      try {
        const updatedData = update(template);
        const dataWithRoutePath = Object.assign({}, updatedData, {
          route: {
            path: route.path
          }
        });
        res.render(template, dataWithRoutePath);
        console.log(`Request path ${chalk.blue(req.path)} loaded successfully`);
      } catch (error) {
        console.log(`${chalk.red('ERR')} An error occurred at render for path ${chalk.magenta(req.path)}`);
      }
    } else {
      renderNotFound(req, res);
    }

    /* const withNamespaces = Object.assign({}, data, {
      settings: {
        'twig options': {
          namespaces: {
            blocks: `${paths.blocks}/`,
            pages: `${paths.pages}/`
          }
        }
      }
    })
    twig.renderFile(template, withNamespaces, (error, html) => res.send(html)); */
  });
});

app.get('*', (req, res) => {
  renderNotFound(req, res);
});


const server = app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});


const watcher = chokidar.watch(paths.templates, {
  persistent: true
});
watcher.on('change', filePath => {
  const relative = path.relative(paths.root, filePath).replace(/\\/g, '/');
  console.log(`File ${chalk.yellow(relative)} has been changed`);
  /* server.close(() => {
    console.log('Server closed');
    process.exit(0);
  }); */
});
