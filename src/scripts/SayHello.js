const MESSAGE = 'Hello World!';

class SayHello {
  hello() {
    console.log(MESSAGE);
  }
}

export default SayHello;
