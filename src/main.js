import SayHello from 'scripts/SayHello';
import 'styles/main';

const say = new SayHello();
say.hello();

if (module.hot) {
  module.hot.accept();
}
