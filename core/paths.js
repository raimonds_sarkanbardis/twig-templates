import fs from 'fs';
import path from 'path';

const rootDirectory = fs.realpathSync(process.cwd());
const resolvePath = relativePath => path.resolve(rootDirectory, relativePath);

const paths = {
  root: rootDirectory,
  public: resolvePath('public'),

  appSrc: resolvePath('src'),
  mainJS: resolvePath('src/main.js'),

  scripts: resolvePath('src/scripts'),
  styles: resolvePath('src/styles'),
  images: resolvePath('src/images'),

  templates: resolvePath('templates'),
  blocks: resolvePath('templates/blocks'),
  pages: resolvePath('templates/pages')
};

export default paths;
