import fs from 'fs';
import {resolve, dirname} from 'path';
import paths from '../paths';

const getData = (path, readFileSync = false) => {
  const resolvedPath = resolve(path, 'data.json');
  let data = {};
  if (fs.existsSync(resolvedPath)) {
    if (readFileSync) {
      try {
        data = JSON.parse(fs.readFileSync(resolvedPath, {
          encoding: 'utf8'
        }));
      } catch (error) {}
    } else {
      data = require(resolvedPath);
    }
  }
  return data;
};

const getComponentData = (componentPath, options = {}) => {
  const {readFileSync = false} = options;
  const templateData = getData(componentPath, readFileSync);
  const globalData = getData(paths.templates, readFileSync);
  const data = Object.assign({}, {global: globalData}, templateData);
  return data;
}

const updateComponentData = template => {
  const componentPath = dirname(template);
  return getComponentData(componentPath, {
    readFileSync: true
  });
}

const getComponent = (componentName, path = 'pages') => {
  const componentPath = resolve(paths[path], `${componentName}/`);
  const template = resolve(componentPath, `${componentName}.twig`);
  if (fs.existsSync(template)) {
    const data = getComponentData(componentPath);
    return {template, data, update: updateComponentData};
  }
  return {};
};

export default getComponent;
