import {resolve} from 'path';
import paths from './paths';
import getComponent from './utils/getComponent';

/* const routes = [
  {
    path: '/',
    component: getComponent('Home', 'pages')
  }, {
    path: '/about',
    component: getComponent('About', 'pages')
  }
]; */

const globalData = require(
  resolve(paths.templates, 'data.json')
);

const mainNavItems = globalData['main_nav_items'];
const routes = mainNavItems.map(item => ({
  path: item.href,
  component: getComponent(item.component || item.label, 'pages')
}));

export default routes;
